package com.queue.array;

import java.util.Scanner;

/**
 * @author mfy
 * @date 2021/9/11
 *
 * 数组模拟队列
 * 环形队列
 */
public class ArrayQueueDemo2 {
    public static void main(String[] args) {
        //最大有效数字为 maxSize - 1
        ArrayQueue arrayQueue = new ArrayQueue(4);

        Scanner scanner = new Scanner(System.in);
        char Key = ' ';
        boolean loop = true;
        while (loop){
            Key = scanner.next().charAt(0);
            switch (Key){
                case 's':
                    //showQueue
                    arrayQueue.showQueues();
                    break;
                case 'a':
                    //addQueue
                    System.out.println("输入一个数：");
                    arrayQueue.addQueue(scanner.nextInt());
                    break;
                case 'p':
                    try {
                        //pushQueue
                        int pushQueue = arrayQueue.pushQueue();
                        System.out.println("取出的数据 = " + pushQueue);
                    }catch (Exception e){
                        System.out.println(e.getMessage());
                    }

                    break;
                case 'e':
                   loop = false;
                    break;
            }
        }
        System.out.println("退出程序！");
    }

   static class ArrayQueue{//TODO

        private int front;
        private int rear;
        private int maxSize;
        private int[] arr;

        public ArrayQueue(int maxSize){
            this.maxSize = maxSize;
            arr = new int[maxSize];
            front = 0;
            rear = 0;
        }

        //添加
        public void addQueue(int num){
            if (isFull()){
                System.out.println("队列满了！");
            }else {
                arr[rear] = num;
                rear = (rear + 1) % maxSize;
                System.out.println("rear = " + rear);
            }

        }
        //移除
        public int pushQueue(){
            if (isEmpty()){
                throw new RuntimeException("队列为空！");
            }
            int push = arr[front];
            //后移
           front = (front + 1) % maxSize;
            return  push;

        }
        //显示所以队列
        public void showQueues(){
            if (isEmpty()){
                System.out.println("队列为空  无法显示");
            }else {
                for (int i = front; i < front + getSize() ; i++ ) {
                    System.out.printf("arr[%d] = %d\n",i % maxSize,arr[i % maxSize]);
                }
            }
        }

       public boolean isFull(){
            //0 1 2 3
           return (rear + 1) % maxSize == front ;
       }

       public boolean isEmpty(){
           return rear == front;
       }

       //有效数字个数
       public int getSize(){
            return (rear - front + maxSize) % maxSize;
       }


    }
}
