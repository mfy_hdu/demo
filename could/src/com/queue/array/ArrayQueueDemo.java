package com.queue.array;

import java.util.Scanner;

/**
 * @author mfy
 * @date 2021/9/11
 *
 * 数组模拟队列
 * 问题：数组只能用一次   添加取出后无法二次添加
 * 解决优化思路： 环形队列
 */
public class ArrayQueueDemo {
    public static void main(String[] args) {
        ArrayQueue arrayQueue = new ArrayQueue(2);

        Scanner scanner = new Scanner(System.in);
        char Key = ' ';
        boolean loop = true;
        while (loop){
            Key = scanner.next().charAt(0);
            switch (Key){
                case 's':
                    //showQueue
                    arrayQueue.showQueues();
                    break;
                case 'a':
                    //addQueue
                    arrayQueue.addQueue(1);
                    break;
                case 'p':
                    try {
                        //pushQueue
                        int pushQueue = arrayQueue.pushQueue();
                        System.out.println("取出的数据 = " + pushQueue);
                    }catch (Exception e){
                        System.out.println(e.getMessage());
                    }

                    break;
                case 'e':
                   loop = false;
                    break;
            }
        }

    }

   static class ArrayQueue{//TODO

        private int front;
        private int rear;
        private int maxSize;
        private int[] arr;

        public ArrayQueue(int maxSize){
            this.maxSize = maxSize;
            arr = new int[maxSize];
            front = 0;
            rear = 0;
        }

        //添加
        public void addQueue(int num){
            if (isFull()){
                System.out.println("队列满了！");
            }else {
                arr[rear] = num;
                System.out.println("array[rear] = " + num);
                rear++;
            }

        }
        //移除
        public int pushQueue(){
            if (isEmpty()){
                throw new RuntimeException("队列为空！");
            }
            int push = arr[front];
            //后移
            front++;
            return  push;

        }
        //显示所以队列
        public void showQueues(){
            if (isEmpty()){
                System.out.println("队列为空  无法显示");
            }else {
                for (int i : arr) {
                    System.out.println(i);
                }
            }
        }

       public boolean isFull(){
           return rear >= maxSize  ;
       }

       public boolean isEmpty(){
           return rear == front;
       }


    }
}
