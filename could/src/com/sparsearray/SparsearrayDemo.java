package com.sparsearray;

import java.util.concurrent.ForkJoinPool;

/**
 * @author mfy
 * @date 2021/9/10
 * 实现sparseArray
 */
public class SparsearrayDemo {
    public static void main(String[] args) {

        //构建原始数组
        int array[][] = new int[3][4];
        array[1][3] = 1;
        array[2][1] = 2;
        System.out.println("原始的二维数组");
        for (int[] ints : array) {
            for (int anInt : ints) {
                System.out.print(anInt + "\t");
            }
            System.out.println();
        }

        //转稀疏数组
        int num = 0;
        int row = 0;
        int col = 0;
        //创建稀疏数组
        for (int[] ints : array) {
            for (int anInt : ints) {
                if (anInt != 0){
                    num++;
                }
            }
        }
        int sparsearray[][] = new int[num+1][3];
        //稀疏数组第一行
        sparsearray[0][0] = array.length;
        sparsearray[0][1] = array[0].length;
        sparsearray[0][2] = num;
        int spaRow = 0;

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
              if (array[i][j] != 0){
                  spaRow++;
                  sparsearray[spaRow][0] = i;
                  sparsearray[spaRow][1] = j;
                  sparsearray[spaRow][2] = array[i][j];
              }
            }

        }

        System.out.println("二维数组->稀疏数组：");
        for (int[] ints : sparsearray) {
            for (int anInt : ints) {
                System.out.print(anInt + "\t");
            }
            System.out.println();
        }

        //稀疏数组转稀疏数组

        int newArray[][] = new int[sparsearray[0][0]][sparsearray[0][1]];
        for (int i = 1; i < sparsearray[0][2] + 1; i++) {
            int i0 = sparsearray[i][0];
            int i1 = sparsearray[i][1];
            int i2 = sparsearray[i][2];
            newArray[i0][i1] = i2;
        }
        System.out.println("稀疏数组-->二维数组");
        for (int[] ints : newArray) {
            for (int anInt : ints) {
                System.out.print(anInt + "\t");
            }
            System.out.println();
        }
    }
}
